﻿# NodeJS - INGESUP - B3 - TP n°6

## Présentation
- TP du cours N°6 portant sur la mise en production
- Implémentation d'une pipeline CI/CD

### Pipeline
- Lint & Test => Stage "Quality" => Tests qualité: lint (qualité et conventions du code) et tests unitaires
- Build => Stage "Package" => Build de l'application
- Heroku => Stage "Heroku" => Déploiement sur le cloud Heroku

### Projet
Utilisation d'un projet "dummy". Il pourra aussi être intéressant d'intégrer le TP de testing une fois la correction disponible

## Lien du git

https://gitlab.com/yoru-san/book-b3

## Auteurs
- Céline BERTAUD
- Fabio CORNEAU